package com.hm.deploytoaws.controller;

import com.hm.deploytoaws.model.HealthModel;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HealthCheckController {
    @Value("${health}")
    private String healthValue;

    @GetMapping("/health")
    public HealthModel getHealth() {
        return new HealthModel(healthValue);
    }
}
