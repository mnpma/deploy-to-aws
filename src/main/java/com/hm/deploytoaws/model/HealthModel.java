package com.hm.deploytoaws.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class HealthModel {
    private String propertyFromYaml;
    private String staticProperty = "Static";

    public HealthModel(String propertyFromYaml) {
        this.propertyFromYaml = propertyFromYaml;
    }
}
