FROM jenkins/jenkins:lts-jdk11
MAINTAINER MP
# docker run -p 8080:8080 -p 50000:50000 -v jenkins_home:/var/jenkins_home jenkins/jenkins:lts-jdk11
COPY plugins.txt /usr/share/jenkins/plugins.txt
RUN /usr/local/bin/install-plugins.sh < /usr/share/jenkins/plugins.txt

EXPOSE 8080
# uncomment this line if you want to attach slaves to this container
EXPOSE 50000
