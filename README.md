### Deploy to AWS EC2 ###

* Demonstrate  Infrastructure as Code
** Deploy the endpoint `http://localhost:8090/health` on AWS EC2 using Jenkins pipeline from a developer station
  
### How do I get set up? ###

* Summary of set up
```text
Go to AWS Console and create Key pair
Save Id key: key-081fe947a01fc760e
Name: ec2-amazon-v1

Find AMI Id : ami-0c02fb55956c7d316 

update main.tf by Id key and AMI Id

Jenkins: 
- add SSH plugin
- add SSH credentials
- update Jenkinsfile
```

 ````shell
#1 Run Jenkins 
cd project_dir
docker-compose up --build --force-recreate
# localhost:8080
# see admin password
# setup Suggested plugins
# setup pipeline job
# Build Now
````

* Configuration
** Jenkins
  [Official Jenkins Docker image](https://github.com/jenkinsci/docker/blob/master/README.md)
  [SSH Pipeline Steps](https://github.com/jenkinsci/ssh-steps-plugin)
** Terraform
  [SSH Keys](https://www.terraform.io/cloud-docs/api-docs/ssh-keys)
  [Utilize hashicorp/terraform image as CLI](https://discuss.hashicorp.com/t/utilize-hashicorp-terraform-image-as-cli/4794)

* Deployment instructions

### Useful links
[Using multiple containers](https://www.jenkins.io/doc/book/pipeline/docker/)
[Spring Boot Docker](https://spring.io/guides/topicals/spring-boot-docker/)
[What’s the Difference Between sh and Bash?](https://www.baeldung.com/linux/sh-vs-bash)
[What is the sh -c command?](https://askubuntu.com/questions/831847/what-is-the-sh-c-command)
[How to use Terraform via Docker Compose for Pros](https://londonappdeveloper.com/how-to-use-terraform-via-docker-compose-for-professional-developers/)

### How to Build, Run / Exec dockers
* Terraform - Dockerfile
```shell
cd terraform_res
docker build --no-cache --tag terraform/alpine:lt --file terraform.Dockerfile .

#  docker run [OPTIONS] IMAGE [COMMAND] [ARG...]
docker run --rm --env-file env.list --name=tf \
 -ti -v $HOME/projects/deploy-to-aws/terraform_res/terraform_vol:/shared_vol terraform/alpine:lt

# work
docker run --name=tf  --entrypoint=/bin/sh -it -d terraform/alpine:lt
# run as detached
# terraform will search *.tf files in $HOME/projects/deploy-to-aws/terraform_res/terraform_vol
docker run --name=tf --env-file env.list \
-v $HOME/projects/deploy-to-aws/terraform_res/terraform_vol:/shared_vol -w /shared_vol --entrypoint=/bin/sh -it -d terraform/alpine:lt


docker volume inspect shared_vol
sudo ls /var/lib/docker/volumes/shared_vol/_data
# /var/lib/docker/volumes/shared_vol/_data

#  docker exec [OPTIONS] CONTAINER COMMAND [ARG...]
docker exec -it tf echo "Hello from container!"
docker exec -it tf terraform -version

# Before execute init, plan, apply -y
# Copy into terraform_vol the following files: main.tf, init_script.sh

# CLEAR
 docker stop terraform
 docker rm terraform
 docker rmi terraform/alpine:lt
 docker volume rm shared_vol
```
* Terraform
```shell
docker-compose -f docker-compose.yml run --rm terraform init

# When Terraform initialises, 
# it creates some working files like .terraform and .terraform.lock.hcl in your project:


```

* Maven
```shell

```

* SSH

```shell

```

* Jenkins
````text

Problem:
Got permission denied while trying to connect to the Docker daemon socket at unix:///var/run/docker.sock:

To solve:
sudo chmod 777 /var/run/docker.sock

````
```shell
docker-compose exec -T terraform init
```

* Docker
[Dockerfile reference](https://docs.docker.com/engine/reference/builder/)
```text
The cache for RUN instructions isn’t invalidated automatically during the next build. 
The cache for an instruction like RUN apt-get dist-upgrade -y will be reused during the next build. The cache for RUN instructions
 can be invalidated by using the --no-cache flag, for example 
 docker build --no-cache.
 
 There can only be one CMD instruction in a Dockerfile. 
 
 If CMD is used to provide default arguments for the ENTRYPOINT instruction, 
 both the CMD and ENTRYPOINT instructions should be specified with the JSON array format.
 
 If you want to run your <command> without a shell 
 then you must express the command as a JSON array and give the full path to the executable. 
 This array form is the preferred format of CMD. 
 Any additional parameters must be individually expressed as strings in the array:
 
FROM ubuntu
CMD ["/usr/bin/wc","--help"]


If you would like your container to run the same executable every time, then you should consider using ENTRYPOINT in combination with CMD. See ENTRYPOINT.

If the user specifies arguments to docker run then they will override the default specified in CMD.




ENTRYPOINT has two forms:

The exec form, which is the preferred form:

ENTRYPOINT ["executable", "param1", "param2"]

The shell form:

ENTRYPOINT command param1 param2

An ENTRYPOINT allows you to configure a container that will run as an executable.

For example, the following starts nginx with its default content, listening on port 80:

 docker run -i -t --rm -p 80:80 nginx
```

VOLUME

VOLUME ["/data"]

The VOLUME instruction creates a mount point with the specified name and marks 
it as holding externally mounted volumes from native host or other containers. 
The value can be a JSON array, VOLUME ["/var/log/"], or a plain string with multiple arguments, such as VOLUME /var/log or VOLUME /var/log /var/db. For more information/examples and mounting instructions via the Docker client, refer to Share Directories via Volumes documentation.

The docker run command initializes the newly created volume with any data that exists 
at the specified location within the base image. For example, consider the following Dockerfile snippet:

FROM ubuntu
RUN mkdir /myvol
RUN echo "hello world" > /myvol/greeting
VOLUME /myvol

This Dockerfile results in an image that causes docker run 
to create a new mount point at /myvol and copy the greeting file into the newly created volume.


**The host directory is declared at container run-time**: 
The host directory (the mountpoint) is, 
by its nature, host-dependent. This is to preserve image portability, 
since a given host directory can’t be guaranteed to be available on all hosts. 
For this reason, you can’t mount a host directory from within the Dockerfile. 
The VOLUME instruction does not support specifying a host-dir parameter. 
**You must specify the mountpoint when you create or run the container.**


```shell
docker rmi deploy-to-aws_terraform:latest
docker rmi terraform:latest

```