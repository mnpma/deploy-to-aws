FROM alpine:latest
MAINTAINER MP
VOLUME shared_vol

COPY terraform_1.1.7_linux_amd64.zip /terraform.zip

RUN unzip /terraform.zip -d /
RUN cp terraform /usr/local/bin/
#RUN cp terraform /terraform_vol/
#RUN cd /terraform_vol && ./terraform -help > /shared_vol/terraform_help

RUN apk add --no-cache ca-certificates curl
#USER nobody
#ENTRYPOINT [ "sh", "-c", "./terraform" ]
#ENTRYPOINT ["sh"]
ENTRYPOINT ["terraform"]
